Prevent code evaluation in org-macro--set-templates
Backported from emacs-29 branch
https://bugs.gentoo.org/927727

commit befa9fcaae29a6c9a283ba371c3c5234c7f644eb
Author: Ihor Radchenko <yantar92@posteo.net>
Date:   Tue Feb 20 12:19:46 2024 +0300

    org-macro--set-templates: Prevent code evaluation
    
--- emacs-28.2/lisp/org/org-macro.el
+++ emacs-28.2/lisp/org/org-macro.el
@@ -103,6 +103,13 @@ org-macro--set-templates
   (let ((new-templates nil))
     (pcase-dolist (`(,name . ,value) templates)
       (let ((old-definition (assoc name new-templates)))
+        ;; This code can be evaluated unconditionally, as a part of
+        ;; loading Org mode.  We *must not* evaluate any code present
+        ;; inside the Org buffer while loading.  Org buffers may come
+        ;; from various sources, like received email messages from
+        ;; potentially malicious senders.  Org mode might be used to
+        ;; preview such messages and no code evaluation from inside the
+        ;; received Org text should ever happen without user consent.
         (when (and (stringp value) (string-match-p "\\`(eval\\>" value))
           ;; Pre-process the evaluation form for faster macro expansion.
           (let* ((args (org-macro--makeargs value))
@@ -115,7 +122,7 @@ org-macro--set-templates
 		      (cadr (read value))
 		    (error
                      (user-error "Invalid definition for macro %S" name)))))
-	    (setq value (eval (macroexpand-all `(lambda ,args ,body)) t))))
+	    (setq value `(lambda ,args ,body))))
         (cond ((and value old-definition) (setcdr old-definition value))
 	      (old-definition)
 	      (t (push (cons name (or value "")) new-templates)))))
