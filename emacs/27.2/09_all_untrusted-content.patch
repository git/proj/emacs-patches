New variable untrusted-content
Backported from emacs-29 branch
https://bugs.gentoo.org/927727

commit ccc188fcf98ad9166ee551fac9d94b2603c3a51b
Author: Ihor Radchenko <yantar92@posteo.net>
Date:   Tue Feb 20 12:43:51 2024 +0300

    * lisp/files.el (untrusted-content): New variable.
    
commit 937b9042ad7426acdcca33e3d931d8f495bdd804
Author: Ihor Radchenko <yantar92@posteo.net>
Date:   Tue Feb 20 12:44:30 2024 +0300

    * lisp/gnus/mm-view.el (mm-display-inline-fontify): Mark contents untrusted.

commit 6f9ea396f49cbe38c2173e0a72ba6af3e03b271c
Author: Ihor Radchenko <yantar92@posteo.net>
Date:   Tue Feb 20 12:47:24 2024 +0300

    org-latex-preview: Add protection when `untrusted-content' is non-nil
    
--- emacs-27.2/lisp/files.el
+++ emacs-27.2/lisp/files.el
@@ -588,6 +588,14 @@
 Some modes may wish to set this to nil to prevent directory-local
 settings being applied, but still respect file-local ones.")
 
+(defvar-local untrusted-content nil
+  "Non-nil means that current buffer originated from an untrusted source.
+Email clients and some other modes may set this non-nil to mark the
+buffer contents as untrusted.
+
+This variable might be subject to change without notice.")
+(put 'untrusted-content 'permanent-local t)
+
 ;; This is an odd variable IMO.
 ;; You might wonder why it is needed, when we could just do:
 ;; (set (make-local-variable 'enable-local-variables) nil)
--- emacs-27.2/lisp/gnus/mm-view.el
+++ emacs-27.2/lisp/gnus/mm-view.el
@@ -466,6 +466,7 @@ mm-display-inline-fontify
 	  (setq coding-system (mm-find-buffer-file-coding-system)))
 	(setq text (buffer-string))))
     (with-temp-buffer
+      (setq untrusted-content t)
       (buffer-disable-undo)
       (mm-enable-multibyte)
       (insert (cond ((eq charset 'gnus-decoded)
--- emacs-27.2/lisp/org/org.el
+++ emacs-27.2/lisp/org/org.el
@@ -1077,6 +1077,24 @@
   :package-version '(Org . "8.0")
   :type 'boolean)
 
+(defvar untrusted-content) ; defined in files.el
+(defvar org--latex-preview-when-risky nil
+  "If non-nil, enable LaTeX preview in Org buffers from unsafe source.
+
+Some specially designed LaTeX code may generate huge pdf or log files
+that may exhaust disk space.
+
+This variable controls how to handle LaTeX preview when rendering LaTeX
+fragments that originate from incoming email messages.  It has no effect
+when Org mode is unable to determine the origin of the Org buffer.
+
+An Org buffer is considered to be from unsafe source when the
+variable `untrusted-content' has a non-nil value in the buffer.
+
+If this variable is non-nil, LaTeX previews are rendered unconditionally.
+
+This variable may be renamed or changed in the future.")
+
 (defcustom org-insert-mode-line-in-empty-file nil
   "Non-nil means insert the first line setting Org mode in empty files.
 When the function `org-mode' is called interactively in an empty file, this
@@ -15827,6 +15845,7 @@ org-latex-preview
   (interactive "P")
   (cond
    ((not (display-graphic-p)) nil)
+   ((and untrusted-content (not org--latex-preview-when-risky)) nil)
    ;; Clear whole buffer.
    ((equal arg '(64))
     (org-clear-latex-preview (point-min) (point-max))
